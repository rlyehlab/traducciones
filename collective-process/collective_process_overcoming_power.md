# COLLECTIVE PROCESS
# OVERCOMING POWER

THIS IS A HANDBOOK ON GROUP PROCESS
FOR EGALITARIAN COLLECTIVES. ITS FOCUS IS
ON THE OFTEN UNRECOGNIZED NEGATIVE
DYNAMICS THAT CAN OCCUR WHEN PEOPLE
TRY TO WORK COLLECTIVELY.

The Common Wheel Collective


## Introducción al consenso

Generalmente, un colectivo que opera por consenso facilita encuentros regulares en los que las propuestas son presentadas y discutidas.
Al final de cada debate, le facilitadorx hará un llamado a la presentación de objeciones; si no se hace ninguna, se dirá que la propuesta ha pasado por un consenso. Aún así, este proceso no siempre garantiza que realmente haya habido consenso, ya que mucho depende de las dinámicas de poder que entran en juego. Por ejemplo, si existen acercamiento individuales a compañeres antes de tiempo y persuasiones en relación a los méritos de la propuesta, esa es una manipulación del proceso, dado que se saltea el foro abierto, que se encuentra en el corazón del consenso. O, si unx compañere influyente o intimidante apoya a viva voz la propuesta o exhibe molestia o impaciencia con quienes presentan preocupación, limitando el intercambio libre de ideas y posiblemente influenciando el resultado final, entonces la decisión no habrá sido tomada por consenso.

Si algune de lxs compañeres no tiene acceso a la información necesaria para tomar una decisión informada sino que debe confiar en las aseveraciones de lxs proponentes en relación a que su plan es acertado, eso, también, invalidará esencialmente el consenso.

La problemática es aun más espinosa cuando las propuestas no son aceptadas. En muchas instancias, si no se logra la unanimidad, el asunto simplemente se dejará de lado y el grupo volverá a su status-quo. Eso significa que el asunto para el que se diseñó la propuesta se mantendrá irresuelto. Eso no es consenso. El consenso necesita que todxs lxs compañeres consideren al resultado de la discusión como marginalmente aceptable. Si algune propone un cambio porque percibe que el problema debe ser tomado en consideración, esa persona no puede ser pasada por arriba simplemente por el bien del acuerdo colectivo.

Bloquear, la prerrogativa de una o más personas de detener una decisión que todo el colectivo hubiera elegido aprobar, es uno de los aspectos del consenso que pareciera universalmente aceptado. No obstante, no significa que una persona pueda mantener como rehén al colectivo según sus caprichos. El bloqueo debe ser utilizado juiciosamente y no como un juego de poder. A menudo, no obstante, lxs compañeres más dominantes aplican presión al grupo para urgir a alguien a que NO bloquee ni exprese disenso. Bloquear pone a une en el foco de atención, convirtiéndolo en un alborotador, particularmente cuando se trata de desafiar a compañeres poderoses que han persuadido a otres de manera previa y privada para seguir con la agenda. Aquellxs compañeres que se han establecido ha sí mismos como líderes de facto (sí, esto sucede todo el tiempo en colectivos horizontales) y que han conseguido un séquito entre el grupo a partir de su carisma y persuasión, o logrando impresionantes logros para la organización, no necesitan recurrir al bloqueo para matar una propuesta. Para elles será suficiente demostrar enojo, irritación o agitación en relación a la acción sugerida, generando desconfianza entre les otres. Algunes compañeres puntuales podrán destruir una propuesta simplemente frunciendo el entrecejo en el momento justo, suspirando de exasperación o riéndose sarcásticamente. Claramente, esto no es consenso.

El consenso no es simplemente el resultado final de un proceso grupal de toma de decisiones, o un momento en el que se produce el voto y éste es unánime, exceptuando bloqueos y desetimaciones. El proceso de consenso debe ser construido en la totalidad de la estructura de un grupo y organización y debe formar la base para todas sus actividades y operaciones básicas. Esa es la verdad para todo colectivo horizontal, incluso para aquellos que aceptan alguna forma de voto mayoritario en la toma de decisiones y, por lo tanto, no se definen de manera estricta cómo operando por consenso.

La premisa básica del consenso, y de hecho de todo grupo horizontal, es que todes les compañeres del grupo son valioses, que la opinión de todes merece consideración y que el input de todes es necesario para que los esfuerzos del grupo den sus frutos, bajo un espíritu de colaboración. En ello difiere del proceso grupal utilizado en organizaciones convencionales, dado que no se define una relación de adversarios donde un lado gana (en general la mayoría pero -en igual cantidad de ocasiones- aquel lado bancado por aquellxs con más autoridad) y otro lado pierde. En el consenso, el debate no está ocupado en defender una posición sino en llegar a soluciones con las que todes consientan. Para que todes den su consenso de manera libre, no debe existir coersión o iniquidades de poder. Por lo tanto la falta de jerarquías y autoridades no es una estipulación agregada a la estructura de colectivos horizontales sino algo esencial al proceso del consenso.


## The Particular Vulnerability of Collectives


The consensus process is based on the assumption that all
members of the collective are making a good faith effort to work
cooperatively, honestly, and in support of one another to achieve the
mutually agreed-upon ends of the group. This expectation of good
will can leave a collective particularly vulnerable, however, to
manipulation by individuals who may seek to use their participation in
the group to steer it in a direction that better suits them or as a
means to further their own sense of importance or control.

We are familiar with the coercive tactics of pushy salesmen:
gaining our trust by empathizing with our concerns and assuring us
that they are on our side, promising to help us by providing us — at
great sacrifice to themselves, they tell us — with something we want
and need. When we fail to appreciate their sincere and hard-won
efforts on our behalf they act deeply hurt and betrayed.

Most of us are wary of salesmen and may not fall for their
pitches. But when we are dealing with a fellow collective member,
someone who is committed to the same cause and who embraces
our shared belief in equality and fairness, we are not likely to suspect
him or her of ulterior motives. Moreover, if one were to express
reservations about the motivations of a fellow collective member, one
might be accused of undermining the mutual trust that is essential to
the collective process.

Unfortunately, we have seen ugly power plays and underhanded
manipulation of the group's loyalties happen again and again in
egalitarian collectives.

Exhibiting stress, anxiety or grave worry is a common way for
manipulators to exert influence, since most of us are conditioned to
want to help someone in distress, and we may be so eager to do so
that we will overlook other priorities just to ease the discomfort as
quickly as possible. By appearing fretful at the possibility that
something might not get done or put upon by having to do so much
himself, a de-facto leader can galvanize people to act without
attention to previously agreed-upon parameters. Similarly, acting
hurt, shocked, or giving the appearance that one is seething with
righteous indignation in the face of a concern that has been raised is
a quick way to silence inconvenient dissent.

The group's most common reaction to a faction or individual who
seeks to sway the collective's will is not, as one would hope, calling
the authoritarian manipulators to task, but gratitude that someone is
taking on the difficult work of running the group and its activities.
These members become complicit in the power-grabbing tactics of
the self-appointed leader(s). Oftentimes, collective members actually
offer these self-appointed elites their loyal support and become
openly distrustful or disdainful of those who question the actions or
authority of the leadership. At this point, the group is not only no
longer operating collectively or by consensus, it has effectively
become a private club.



## There's Hope


It is our belief and hope that virtually all problems in collectives
can be overcome by applying compassion, tolerance, and patience,
and by being thorough and even-handed in our thinking.

Recognize that some people are a big pain in the ass, but that
doesn't mean that they are agent provocateurs. And even if they are,
the best way to deal with disruptors in either case is probably to give
them a certain amount of leeway to be themselves, to let them carry
on instead of demanding that they cease. Provocation can be
defused simply by not engaging it.

If the level of annoyance is such that it cannot simply be
tolerated, then talk it over with the person: let him know what
behaviors of his are causing problems for you and help him find ways
to change them. Actions that we may see as negative usually arise
from a need on the part of the person engaging in them: whether it's
the need to be listened to, to get to the bottom of issues, etc. Our job
is to help find a way for the person to still be able to have his need
met if he agrees to drop the offending behavior. The only way to do
that is to talk to him. People who are being a nuisance don't see
themselves that way. They have a reason for what they're doing. Try
to learn their perspective. Some people act in bad faith. Learn their
perspective too, so you can expose it for what it is.

If we care, genuinely, about mutuality and inclusion, if we believe
this to be one of the basic reasons why we want to work for a better,
more just world, then we need to ask ourselves a simple question: if
this person whom we cannot stand were a member of our family,
would we turn her out into the street? Or would we put our hearts
ahead of our frayed nerves and learn to deal with her annoying
character traits? Likewise, if a member of our family spoke frankly
and unkindly to us ("Look, you're driving me nuts: could you please
just shut up?"), would we demand that the whole family intervene to
sanction her?

Because most of us tend to throw caution or our sense of
fairness to the wind whenever someone has made us very angry, we
recommend having clear and concrete protocols in place that can be
called upon whenever conflicts, differences in approach, or hurt
feelings crop up. Rules, however, though they can help us keep our
priorities in order, cannot take the place of basic human qualities:
compassion, patience, tolerance, and the desire to seek out the truth.
Without our humanity as our foremost guiding principle, no set of
guidelines can come to our rescue. We need to always keep
referring back to what's important when striving to make decisions on
how to proceed, especially in a difficult or trying situation. What's
important is not the work of the group nor effecting political change:
it's the fact that we care about and value one another, as we do all
people. That's why we're in the struggle for social justice, after all.

Some groups may have no patience for tending to the weak and
the whiny. They may feel that those who do not contribute or are
slowing or bringing the rest of the collective down need to move on
and get out of the way. Any group can choose that path, of course.
But if they do, they have a responsibility to do so honestly and
openly. Such an enterprise can no longer call itself consensus-based
nor egalitarian. The premise of consensus and equality rests firmly
on the belief that everyone in the group is valued and necessary to
maintain the integrity of the whole. It presupposes a shared effort
and mutuality which cannot be undermined by picking and choosing
who is valuable and who is not.

Despotism by the collective, which rests on groupthink, whereby
everybody has to agree, no one can dissent, and those who dissent
or who simply are not well liked are outta here, does not equal
consensus.

## What's a Lone Person to Do?


If you're reading this book because you see a problem in your
collective that you think should be addressed, you may well be alone
in your quest. If you've actually raised your concerns with the group,
you may suddenly find yourself the outcast, with the rest of the
members possibly either openly hostile or utterly indifferent.

It's all well and good to say that all the people in a collective
need to take responsibility for the group's functioning in order to
avoid power inequalities and ensure a true spirit of consensus and
collectivity, but if you're just one person, and the group is in fact not
taking responsibility and is allowing a self-appointed leader or faction
to steer decisions (including the newly-arrived-at conclusion that
perhaps you are no longer a valued or wanted member), what can
you alone do?

We wish we had the answer. (Our own personal solution has
been to stagger away, blinded by pain, to tend to our wounds in a
dark corner, wondering what hit us and why. We also decided to
write a book on collective process.) This chapter is more than
anything a cautionary note. Because you have read the contents of
this book (and hopefully a number of others) on the topic of collective
function and dysfunction, you may consider yourself armed with an
arsenal of information and insight on what is going wrong with your
group. You may feel confident that you can make a good case to the
membership for the need for self-analysis and reassessment of
priorities. But that doesn't mean you won't still find yourself alone and
the subject of attacks and slander.

Evidence from books is very unconvincing to people who won't
make an effort to try to understand the situation or the underlying
problems, and even less so to anyone who has already reached a
conclusion based on rumors, speculation, and innuendos. There is a
saying, which unfortunately is all too often appropriate in collectives
that are experiencing conflict: "My mind is made up, don't bother me
with facts."

In many cases, people who feel they have carved out their little
corner of power are not going to give it up easily, no matter how
trivial their sphere of influence may seem. If you threaten the
hegemony of someone in a position of some authority, whether his
leadership is overt or subtle, (or even if you haven't done anything
that could be construed as a threat but he thinks there's the potential
that you might, perhaps because you've been outspoken) you may
very well see another side of him, one with bared teeth and hissing.

It has been suggested that rather than going it alone one should
set out to build a coalition, persuading each person individually,
through private conversation, before making one's concerns public.
This is classic political strategizing. We feel very ambivalent about
this. On the one hand, it might work, and it could be preferable to
exposing oneself as a sole target to a verbal battering. On the other
hand, it's a manipulative tactic that could be characterized as sleazy,
depending on the amount and quality of the persuading involved.

Furthermore, you will always be out-sleazed by the other party if
she is willing to go further than you are. This is not a competition
worth entering into unless you're willing to go over to the dark side.
After your fellow collective members have figuratively beaten you up
with personal attacks, vilification, and calls for your banishment, we
think you will want, at least, to walk away with your integrity.


## Red Flags to Guard Against



The following is a by-no-means-exhaustive list of behaviors that
should send up a red flag among collective members that the group's
dynamics may need to be reexamined to ensure equal participation
(and to stop divas and ego-maniacs in their tracks).

### Group Behaviors

1. Meetings are poorly attended and those who do attend appear to
be sullen and bored, letting a self-appointed leader set the agenda
and do most of the talking. This is a sure sign that people have given
up on the possibility of having meaningful input into the group's
direction.

2. Meetings are not held at all, or not for months, because of lack of
interest. (Note: Some groups get together on a regular basis to work
on projects. These may count as informal meetings if decisions and
issues are discussed in the course of the work. That's okay: it
doesn't signal lack of participation.)

3. Someone or a faction denigrates meetings (boring, take up too
much time, people have better things to do, meetings are for people
who are only interested in process and not in actually getting things
done) so that they are rarely held, hurried, or badly attended. As a
result, one small group or individual can make decisions on
his/her/their own without having to consult anyone else.

4. People walk on eggs for fear of upsetting the "leader." People
chastise others for having upset the "leader".

5. Someone or a faction derides the idea of using a facilitator or an
agreed-upon process, implying that "our group" is above needing all
that.

6. Unsubstantiated rumors and gossip, especially attacking someone
for being racist or sexist (hard to defend against) or for unspecific
offenses, such as being "uncooperative," "unreasonable," or
"disruptive" (hard to prove or disprove).

7. A sustained campaign to discredit someone, with accusations
such as "thief," "liar," and "control freak" being tossed about without
substantiation or clearly trumped up (i.e. a person who borrows or
loses something is declared a thief and a ban is called for).

8. A petition being circulated for members' signatures that vilifies
someone. People signing such a petition without any first hand
knowledge of the accusations — often in an attempt to be helpful: "I
don't want that person to destroy the group!" (Or to avoid angering
the accusers and becoming themselves the subjects of the next
petition.)

9. Constant shit-talking about people formerly associated with the
group, even in a seemingly humorous vein.

10. Calls for banning cropping up whenever there's a problem.


### Individual Behaviors

1. Acting exasperated that someone would waste the group's time
with trivialities.

2. Crushing dissent by fabricating distracting excuses or creating a
smokescreen.

3. Trying to create a feud by consistently slandering someone behind
their back or baiting them to their face. (For instance: is there
someone who takes every opportunity to always complain about the
same person? "He/she is a stalker/a sexual harasser/a
sexist/crazy/out to get me, etc.")

4. Using outright intimidation such as staring down, yelling,
histrionics or acting as if one is (barely) suppressing indignant rage.

5. Acting wounded or victimized when one is actually the aggressor.

6. Acting wounded or outraged whenever someone makes a
reasonable request, like asking for accountability of an expenditure.
(Extra-red flag: Does this person consider herself to be so far above
the rules that govern the group that she might actually be
appropriating the group's funds or other resources?)

7. Making oneself indispensable by not allowing anyone to help or
have access to the information they would need in order to help.

8. Suggesting (or insisting!) that fundamental principles should be set
aside to deal with a crisis (or to appeal to important constituencies,
like sources of funding).

9. Having no patience for fundamental principles (implying that they,
or ideals in general, are childish).

10. Relishing verbal arguments with those less knowledgeable or
more vulnerable just for the glee of crushing them.

11. Demonstrating contempt for other people's ideas or their right to
express them (i.e. by scoffing, ridiculing, or belittling). Not to be
confused with honest debate, which engages. Contempt only
silences.

12. Controlling situations with fear by flying into a histrionic rage at
insignificant provocation (i.e. a group didn't put away chairs after a
meeting, people working on a project didn't call before stopping by).

13. Controlling situations with fear by predicting dire consequences.
People who are worried or perceive an impending crisis are much
more likely to succumb to manipulation.

14. Creating and spreading doomsday scenarios while setting
oneself up as the lightning rod to deflect them.

15. Paranoia. Ascribing nefarious underlying motives to someone's
apparently innocent or merely uninformed actions. Going on the
attack is often the most effective way to avoid having to answer for
one's own behavior (e.g. someone who borrows without asking the
right person is a "thief" and should be banned; someone who adopts
a dog and moves it into the space obviously thinks the group's space
is his own private home).

16. Creating self-fulfilling prophecies that serve one's goals. (For
example: repeatedly stating that the neighbors are becoming less
and less tolerant of loud punk rock shows. )

17. Flaunting one's knowledge (esp. of anarchism, collectivism,
radicalism) to set oneself up as the go-to person for advice on how to
proceed.


## The Need For Kindness


Although collective members should not subject one another to
fake sentimentality and cloying praise, the shared effort of being in a
collective presupposes good will and genuine consideration for each
person involved. If the basis for interactions among the group is not
kindness, tolerance, and acceptance in spite of unavoidable flaws,
then there is a dynamic at work which does not support consensus.
The basis for consensus is not shared decision making (that's an
outcome), but fundamental respect for the concerns of each member
and for the person herself or himself. Whenever there is bullying,
ridiculing, or grandstanding, there is no consensus.

In "The Problem With Politeness" we stress the need to allow
members to express anger and other unpleasant or difficult emotions
and opinions. It's okay for a member to be angry, annoyed, or wrong.
People make mistakes; the collective should consider that a normal
part of functioning. Those who commit blunders should strive to
correct them and then move on. What is not okay is bad behavior
that is intentional: that is, it has been devised to create a particular
outcome, whether it's to intimidate dissenters, prove a point, or
demonstrate one's supremacy in a given area. It's also not okay to
upset other people just to amuse oneself.

Even those of us who elect to participate in egalitarian
collectives have been living in a society that places people in
positions of authority and submission with respect to one another.
Most of us understand that equality means neither giving nor taking
orders and rejecting any form of established hierarchy, but when it
comes to informal hierarchies, collective members sometimes fall
back onto what they've been accustomed to by mainstream culture.
For instance, if someone seems particularly knowledgeable in a
given area and willing to take on high-visibility tasks, he is sometimes
allowed to attain a position of informal leadership. What makes this
possible (in addition to garden-variety laziness) is the mainstream
notion, especially difficult to shake among those of us who took pride
in doing well in school and being recognized for it, that people should
be praised and acknowledged for their talents and successes. In a
truly egalitarian group, everybody contributes according to his or her
ability and availability, and no one expects to get or take credit for his
or her achievements. Hero-worship is incompatible with consensus.
All accomplishments are somehow built on someone else's
shoulders.

Loyalty, which on its face might seem like a good thing, has no
place in egalitarian collectives that strive to be fair to all members.


Loyalty is what causes us to stick up for someone close to us, even
to the detriment of another, when we know our crony is wrong. Or to
overlook facts and forego investigating a matter even when it would
mean clearing an innocent person of wrongdoing. Fairness requires
that we listen to all and consider all possibilities before arriving at an
opinion.


## Creating Pariahs


One of the ugliest and most reprehensible tendencies that we've
seen in egalitarian collectives is the creation of pariahs: A small
group decides that some individual is undesirable, then he is singled
out for vilification and expulsion. This practice might seem odd for
groups supposedly founded on equality, mutual respect, and
acceptance, but it happens remarkably often. In fact, this matter
deserves a much more thorough treatment than it will receive in this
brief chapter.

Often this process of expulsion is justified by reference to the
anarchist notion of "banning." According to a typical anarchist vision,
people will live or operate in small groups with no leadership, making
all community decisions by means of direct democracy. (In other
words, everyone should be able to participate in such decisions and,
ideally, consent to them.) If somebody somehow sabotages the
community or otherwise causes or threatens serious harm, there are
no police or other authoritarian forms of enforcement to handle the
matter; therefore, the best way for the community to deal with the
offender is to simply, democratically banish her. This practice is said
to be less authoritarian than the conventional methods of criminal
justice and attendant imprisonment, since the person is still free to
seek out association with other communities. The crucial factor that
is often overlooked by present-day collectives is that banning is
meant to be reserved for extreme, dangerous, or criminal behavior,
not as a way to get rid of someone whom some group members
simply find annoying or inconvenient.

It's normal for people sometimes to be obnoxious or awkward.
The basis for collectives founded on equality is that people have the
right to be themselves, regardless of whether their attitudes make
them popular or not. That is not to say that members have to accept
being mistreated by boors. If somebody is bothered, he or she
should let the offender know that such behavior is bothersome and
ask that it change. It may not, in fact, change, in which case these
two people simply must find a way to put up with each other. Human
interactions are rarely perfect.

What so often happens, however, is that one or both people will
make a federal case of the issue, start slinging accusations fast and
loose, and demand that the collective intervene to remove the
supposed culprit. It is not uncommon for members to be sleazily
manipulated so that one side might gain advantage over the other. A
hapless person who wouldn't think of devising strategies or
masterminding plots may suddenly find that she is universally hated,
perhaps without even knowing why. Sometimes secret meetings are
held, without the knowledge of the accused, at which the attendees
will hatch a plan to ostracize her. Usually, this is done for no other
reason than that the complainants are too cowardly to confront the
person directly and simply ask her to alter her demeanor.

Many times a person who is expelled does not even know what
he has done wrong and might very well have corrected himself if only
he'd been told about the offending behavior. Too often groups gang
up against someone only because he has awkward social skills and
unwittingly comes off as impolite or bossy. Do we need to say that
this does not constitute consensus? We've seen junior high students
who behave more maturely.

An uglier form of creating pariahs occurs when a domineering
member or faction intentionally seeks to discredit and eject someone
whom they consider a threat to their hegemony. Sometimes,
someone is targeted this way after she has been outspoken in
condemning the control that the self-appointed elite has wrested from
the collective. In other cases, however, the targeted person may
have merely insisted that the group follow proper democratic
procedure. If taken seriously, that recommendation might have the
potential of removing power from the leading faction — therefore, it
must be suppressed.

The easiest way to impeach the credibility of a dissenter is to
accuse him of having a personal grudge against the person he is
calling to task. The manipulator can then bait the dissenter with
personal insults, and if the poor soul is ruffled and responds in kind,
our Machiavelli will have proven her case: "See? He is just out to get
revenge on me — that's what all of this has been about!"

There is never a wrong time to call into question someone's
actions as they relate to the integrity of the collective's process. In
fact, it is every member's responsibility to do so if and when he feels
the situation calls for it. Unfortunately, few people ever do. People
find it easier not to stick their necks out to speak out on what they
think is right. They may even join in the condemnation of a dissenter,
because they don't like to have their little bubble jostled. They may
readily agree that the troublemaker is not raising an issue but making
a personal attack. Consensus cannot operate in such an
atmosphere. It's likely that anyone who makes waves under these
circumstances will find himself out the door.

It is the responsibility of all collective members to listen carefully
and consider every matter that is brought to their attention, and to
hear from all sides. Members should assume that every concern is
sincere and treat it as such, but, particularly when one person's
concern involves condemning another individual, everyone in the
collective has to make every effort to get to the bottom of the issue
without jumping to conclusions. Ask questions. Investigate. Look to
possible motives to help you ferret out the truth. This is almost never
done. People are usually all too happy to jump on a bandwagon of
character assassination and are unlikely to be dissuaded from
whatever stance they have chosen.

In cases of outright nastiness or bullying, it's appropriate for the
collective to help address the behavior (although it still does not
mean the offender should be summarily expelled!). Rarely, however,
does the group come to the defense of an aggrieved member. As
long as group censure consists of dumping on an unpopular person,
especially if it's by e-mail or out of the individual's earshot, then
people gleefully jump in. But when it comes to confronting a bully,
then — poof! — everyone disappears. Even if the bully has been,
until that point, generally acknowledged as such, when somebody
actually asks for help in calling her to task, suddenly nobody
remembers having had any problems with her.

Too often, ugly banishments happen because the collective has
no guidelines for dealing with disagreements or dissension. In the
absence of a grievance procedure or a forum in which differences of
opinion may be openly discussed, the only options for the group are
either trudging along in some unstructured, undefined manner, with
everybody swallowing whatever concerns they may have and silently
suffering any insults, or forcibly expelling whoever brings up a
problem. In such situations, the promise of inclusion and openness
intrinsic to a consensus-based group has been subverted and
narrowed down to Shut Up or Get Out.

Sometimes, however, even when it seems that the right rules
and guidelines are in place, these can be ignored or rendered
useless. Especially in a smaller group, it is not all that uncommon for
the rules to be overtly disregarded as members decide that those
regulations are nothing more than technical trivialities. Thus,
regardless of the rules, the individual who has been vilified or ousted
has little recourse when the whole small gang (which might call itself
a collective) has simply turned against her. Almost inevitably, she will
end up giving up the struggle because it just doesn't seem worth it to
dredge up rules that nobody cares about, simply to remain among
people who obviously don't want her around.

Established rules can also be easily subverted through the usual
techniques of manipulation, as described in other chapters. A group
might earnestly intend to follow the established procedures for
exploring grievances or granting due process, yet those procedures
will become irrelevant if the whole collective has already been
convinced of the accused person's guilt. Unchecked binges of
character assassination and rumor mongering can psychologically
nullify many "fair trials" before they ever happen.

Ironically, some people use the belief in anarchism as their
excuse to flagrantly ignore rules that were designed to ensure
fairness and democracy. Anarchists who break the rules might go on
the defensive by saying that they don't always have to follow the law,
because they are anarchists. Yet, while it may be true that anarchists
can reserve the right to reject laws that they think are unjust or are
the product of an unjust system, anarchists must also reach a
collective understanding about basic democratic principles.

Rules can become very important, not simply because they are
the rules, but because they can serve as guidelines for achieving
democracy. Those guidelines might be very much needed during
harsh or complex conflicts, when people are more easily confused or
misled into forgetting the most basic principles or even basic logic.

Perhaps someday, everyone will have a strong enough
conviction in — and knowledge of — true democratic principles never
to be misled (or to do the misleading, for that matter). In some golden
age, perhaps after the revolution, everybody will be so
psychologically and socially advanced, that it will simply be
unthinkable — and impossible — for them to contribute to the
creation of pariahs or other acts of collective injustice. Yet, in the
here and now, we probably should do everything we can to keep
those tendencies in check.


## Staying True to the Mission


Many egalitarian collectives consist of activists working to
achieve a just society and were formed for that purpose. Even
collectives that don't have specific political aims have made a
commitment to social justice by virtue of being anti-authoritarian and
pursuing equality as a fundamental goal. It should be obvious that
internal power plays, deceitful back-room plotting, rumor-mongering,
and marginalizing or ridiculing are behaviors that do not befit a group
fighting for fairness and against oppression. Yet, people in collectives
do these things all the time, usually without even inviting a raised
eyebrow.

Collectives that incorporate as non-profits are required by law to
draft a mission statement letting potential supporters know about the
work that the organization exists to achieve. Fulfilling the mission is a
non-profit's legal reason for being (as well as the reason it doesn't
have to pay taxes), just as a for-profit company's all-consuming
purpose in life is to make money for its owners. Most collectives have
no such mandated requirement, but it's still a good idea to compose
a mission statement to refer back to whenever a decision needs to
be made on how the group should act in a given situation. This
position paper should spell out the fundamental belief that the
collective must operate internally by the same high standards of
fairness and democracy that it is working to bring about in the larger
society. If it fails to do that, then it has failed in its most basic goal.


## Respect for Differences


Many collectives are aware that they need to do better in addressing
racism, sexism, and homophobia within their own ranks, but too
many fail to address the reality that lack of respect for differences
does not start with its ugliest and most glaring manifestations but is
present whenever room is not made for another person's viewpoint,
situation, or life experience.

Prejudice does not come in separate compartments. It's not
okay to be against racism, sexism, and homophobia but be
indifferent to xenophobia, ageism, nationalism, classism and the
myriad other ways that people are suspicious of and discriminatory
toward one another.

The hand-wringing and self-blame that collectives engage in as
an attempt to address their own internal problems with insensitivity
are unlikely to yield useful results. Tolerance begins with the
acknowledgement that people other than ourselves may see things
differently than we do, and suspending judgment while those with
whom we may disagree or whose point of view we may not
understand are given a forum to explain their perspective and are
actively listened to. No one can presume to know how someone's life
has shaped him or her. Group dynamics fail to respect differences
whenever assumptions are made about another person.

Collectives that are built around a particular issue are often quite
homogenous. Members would like to embrace differences, in theory,
but when they're actually confronted with someone whose life is
unlike theirs, many find it difficult to see beyond their own limited
experience. A dissimilarity as slight as an awkward social manner,
imperfect language skills, or a reticent personality can be enough to
cast someone as weird or tiresome, and her opinions therefore pre-
judged as unimportant. When we do poorly even at accepting
personal differences and quirks, how can we expect to reach out to
one another across broader differences that arise from race,
ethnicity, class, sexual orientation, and gender?

In a collective that is, for example, made up primarily of college
students or recent graduates, an older person with a family to take
care of can be shut out of the group's work simply by scheduling
meetings at night, when he has to be home to put the children to
bed. Members' disabilities are also often unacknowledged by healthy
people: it's hard to put oneself in someone else's shoes and realize it
may be hard for a person to attend regular planning for events or
work late hours. When a member cannot contribute fully to a group's
activities, he may be left out merely due to careless disregard for his
difficulties: "Well, you weren't there so we decided to do it this way."
Or, worse, groups may consciously and deliberately marginalize
those who don't do as much work or are not present as often, without
giving any consideration to the individuals' circumstances. Illness,
family, work commitments, and financial situations are all differences
that an egalitarian collective must attend to if it is to truly operate by
consensus.

Members of any group who don't have a computer are often
rendered into nonentities because they cannot participate in email
discussions. Many times no one even bothers to keep them apprised
of events and meeting times. A computer is a tool that costs
approximately $1000, plus a monthly internet subscription. Assuming
that everyone in a group, especially a political collective dealing with
issues of economic inequality, should be able to afford such a luxury
is completely at odds with the realities that social activism exists to
address. A collective cannot function by consensus when some of its
members are systematically excluded from its activities.

On a related matter...

Using ugly societal ills like racism and sexism as a pretext to
assassinate the character of perceived enemies is reprehensible.
When a fellow collective member has acted inappropriately, his
particular actions should be addressed by the complainant. Calling
them a sexist, even when it's arguably true, is unhelpful in resolving
conflicts. Such charges are impossible to defend against: being
sexist is too ugly to be excused, therefore no one can go to the
person's defense without appearing to condone sexism, and too
unspecific to be refuted. As a result, all dialogue, which is necessary
in order to come to an understanding of the issue and seek fair
solutions, is silenced. An allegation of sexism or racism can be used
as a ploy specifically to silence dialogue and force group censure or
ostracism against an undesirable individual. If, instead, an offender is
confronted with specific bad behaviors, the possibility exists that he
will understand his mistakes and work to rectify them. After that
hurdle has been crossed, it may well be appropriate to address
whether his actions were the result of broader racist or sexist beliefs
and to discuss the role that sexism and racism play within the
collective's interactions.


## Personal vs. Group Issues

Sometimes, two people caught up in a personal and emotional
kind of war will insist on dragging the whole collective into their
squabble, each (or sometimes only one) person demanding that the
group censure the other. The person who has greater power within
the group, a stronger personality, or the ability to make the best case
for being the most aggrieved might then very well succeed in
gathering an indignant, angry mob to rally against the other party.

It is sometimes helpful for a small number of collective members,
perhaps one to three, to intervene as intermediaries between the
warring parties and help them find an appropriate means to resolve
the conflict, at least to an extent that will allow them to continue
functioning as collective members. For instance, it may be useful to
find neutral mediators outside the group. But it is altogether
inconsistent with the spirit of consensus and egalitarianism, which
presupposes equal respect for each individual and his or her
contribution to the group, for the collective to act as judge and jury
(or bloodthirsty villagers carrying torches) in a situation that is
emotionally painful for those involved and about which the collective
cannot and should not know all the details.

Public conflict resolution, while certainly a better alternative than
jumping to collective conclusions and decisions based on rumors and
innuendo, puts the parties in the embarrassing position of having to
explain private choices (of which they may not be particularly proud)
in front of everybody. This tactic is likely only to lead to
defensiveness, refusal to yield one's ground for fear of losing face,
and further hurt feelings.

A collective may come up with the argument that internal
disputes harm the image of the group to potential outside supporters
and must therefore be suppressed by distancing one of the parties
from its activities. Yet, this idea is highly authoritarian, and it is likely
to do greater damage to the collective by breaking it apart rather than
working to bring it together. Moreover, it leads us to the logical
conclusion that the best way to preserve harmony in the group is
simply not to tolerate conflict.

A converse sort of problem also occurs fairly often: Someone
raises a legitimate grievance about the inappropriate way another
member is conducting herself within the sphere of the collective's
activities, then finds himself being accused of bringing the complaint
up to the collective merely because of a personal dislike.

This instance involves an abuse of the collective process,
usually by a self-appointed leader who does not wish to answer for
her actions — who will therefore seek to distract from any criticism by
claiming that the complainant has a personal problem rather than a
legitimate concern. And soon, the poor soul who had the audacity to
call the leader to task might find himself slandered, vilified, or
attacked with verbal invectives meant to frighten him into submission.

At this point, some well-meaning collective members might
respond to all the interpersonal tension by urging everyone to chill.
They might even spout a bunch of well-meaning platitudes such as,
"What's important is the group's work" (which should not be sidelined
by "petty bickering," of course). And to uninformed passersby, this
might seem like a good assessment, a reasonable answer given in
the interest of peace. In truth, however, such a reaction is simply
callous and insensitive. It's symptomatic of the kind of
thoughtlessness that results when gullible people allow their leader
to manipulate them. (Although, that's not to say that it can't also
sometimes be used as a deliberate tactic as well...)

We believe that in this kind of situation, the collective must
simply encourage the dissenter to speak up. The group should not
allow a dissenting opinion to be stifled simply so that they can avoid
further conflict. That is a false kind of peace, a perpetuation of
injustice that does not suit a group that's (supposedly) seeking to
create a more democratic society.


## Some cardinal points to keep in mind when conflict arises in a group


1. Do not draw any conclusions about an issue without hearing from
both sides. Hear each side out to the extent that each feels is
necessary (i.e. don't assume you've heard enough just because
someone seems tiresome, pedantic, or emotional). Talking to a
friend of a person involved in a conflict is not the same as getting the
lowdown straight from the horse's mouth.

2. Although you may feel it is your duty to throw your support behind
a friend or close ally who is in distress, giving emotional support is
possible — and desirable — without having to draw conclusions or take
sides.

3. Corollary to #2. Regardless of who you believe is right or wrong on
a given issue, give emotional support. It is not okay for the feelings of
the people involved to be trampled on, especially if someone is
clearly suffering, even when one or both of the parties are acting like
jerks. It is especially not okay to jump in and join the faction doing the
stomping on someone's hurt feelings.

4. Assume that every concern is legitimate and address its
substance, even if the tone or context in which it is delivered seems
overblown, emotional, or vindictive.

5. Corollary to #4. Do not dismiss concerns just because the manner
in which they are brought up seems strident or out of place. It is one
of the shameful practices of the adversarial court system, which we
don't want to emulate in our own collectives (at least not in this
respect), to discredit complainants who are emotional or enraged.
For centuries, women's grievances, in particular, have been
successfully shunted aside by overbearing men by claiming that a
woman who is outraged to the breaking point by the injustices and
abuses she has had to suffer is hysterical. (Keep in mind that men
can be very emotional too, and just as readily dismissed for being
so.)

6. Never assume that someone who is raising a concern is just
wasting the group's time. (That can happen, of course, but, at worst,
the outcome of such a situation will simply be a certain amount of
time wasted.) Much more often, someone who feels threatened by
the concern raised will try to persuade the group to squelch it on the
grounds that it is a time-waster.

7. If a concern is in fact taking up too much of the group's time,
create a subcommittee to look into it. The subcommittee should
include the person raising the concern and at least three other
people who are neutral or uninvolved in the issue but who are willing
to take the time to ferret out the facts and study them thoroughly.


8. Sometimes someone (or a group) can be so controlling or self-
involved (often without even realizing it) that he sees any
disagreement with his chosen course as sabotage or disruption and
will react angrily to what he sees as an unnecessary obstacle being
created. This is a very common source of conflict in collectives. The
solution is to treat every concern that is raised as legitimate and to
address it as such. There are often fundamental differences in the
basic values or beliefs of group members that get swept under the
rug in a flurry of angry accusations and are only brought to an end by
driving out or expelling the weaker faction or individual. This is a
terrible breakdown of collectivism and should never be viewed as a
successful resolution to a conflict.

9. Be the solution. Volunteer to create a committee to look into a
problem and, after thorough study, recommend solutions. Volunteer
to seek outside mediators. Talk to both sides to try to understand
each point of view.

10. Instead of listening to empty accusations, look for plausible
motives for people's behavior. When someone is accused of acting a
certain way because he is "crazy," that just does not hold any water.
People usually act badly either because they are upset, insecure,
frustrated, or afraid, or because they have something to gain by that
behavior. Why would someone who has nothing to gain go around
sabotaging or undermining the group's work? Could it be that they in
fact have a legitimate concern they feel needs to be raised and are
only being painted as saboteurs by someone who in fact has
something to gain (such as consolidating his own power) by shutting
them up?

11. A solution to a conflict does not have to — and should not — assign
blame nor declare a victor. When conflicts arise, emotions often run
high. People who feel they have been wronged or mistreated can
react badly. Often, one side (or both) has become so overwrought by
the conflict that she does not want to resolve the problem but merely
crush the perceived offender. It is necessary to create an
atmosphere where both sides can come back to the group relatively
whole; that can only happen when all the issues have been
thoroughly addressed and resolved to an extent that both parties can
live with.

12. Not assigning blame does not mean not acknowledging the
wrongs that have been visited on either side. When people are not
made to feel that they are under attack, but that their concerns will be
genuinely listened to, they are much more likely to admit their
mistakes. Create a means for people who may have acted badly to
make amends, so that everybody can move on. (But do not be the
judge and jury. People can only honestly make amends for errors
that they acknowledge. No one can be forced to admit she was
wrong if she does not in fact believe it. It may be that someone who
is adamant in her position is in fact correct in her claim that she has
been unjustly vilified. A situation that is still in this stage has not been
thoroughly dealt with yet.)

13. A conflict between two people who were previously close friends
or have been involved in a romantic relationship should never result
in the group taking sides against one or the other party. The facts of
the conflict that involve the group as a whole should be addressed as
such (i.e. s/he has been excluding me from activities; badmouthing
me within the group; will not leave me alone when I am doing work
for the group, etc.). The group should absolutely not become
complicit in eliminating the former friend or partner from the
complainant's life by driving him or her out of the collective. It should
become especially obvious in such a case why assigning blame is
fruitless: people who have been hurt sometimes do stupid or cruel
things. There's no need to rub their faces in it.

14. People become involved in conflicts because they have some
unaddressed need. Find out what the need is and determine a way
to address it, with the collaboration of those who are in
disagreement. That is the only way to resolve the conflict: it needs to
be addressed, worked through, and straightened out.

15. Anytime someone is kicked out of the group or leaves voluntarily
in order to stop a painful conflict, there has been a terrible
breakdown, not a conflict resolution.


## Relinquishing Control of Projects and People


The egalitarian group affords its members little opportunity to
control other members or the group itself. Because there are no
leaders, no one is in a position to force another person to act or
refrain from acting in a given situation; only the collective as a whole
can intervene to limit unprincipled behavior. Since the entire
collective has to become involved in order to restrict someone's
autonomy, such a measure should be undertaken only if the behavior
in question is extreme. (We have seen many instances in which
small gaffes are trumped up into serious charges as a way of
exercising control, but that's another topic.) In many collectives, we
are likely to encounter some people who have annoying quirks,
others who are chatterboxes, and others who just don't think before
proposing stupid ideas. But these are not the egregious kinds of
behavior that require official control; galling as they may seem at
times, they must be allowed to exist.

When collective members try to force a desired outcome
according to personal desires, taste, or style, they are basically
violating the principles of maximum autonomy and free choice. This
tendency will almost always lead to arguments and ruffled feelings.
(The corollary to this is that group members have a profound
responsibility not to make themselves a nuisance to others.) A truly
egalitarian collective will likely not be smooth or harmonious (though
it may be loving and collegial), but highly heterogeneous, rife with
rough spots and bumps.

In an egalitarian group, not everybody has to agree or like each
other or approve of the work that is being done; they merely have to
consent to it. This means that unless something is really important or
central to the values of the organization, the wisest course is often
just to let things be. That can be hard to accept when we have been
accustomed to value results over all other considerations.

Almost all people who come to the movement for social justice
were brought up and have been functioning in conventional society,
which presupposes supremacy of one person over another according
to status or perceived superior ability. Whether we mean to or not,
we bring these biases and expectations with us when we agree to
join groups that operate according to equality and collectivism.

Those of us who are accustomed to emerging as natural leaders
(for instance, those who've been successful in academia) may have
an unacknowledged belief that others will readily recognize our
wisdom and defer to it as a matter of course. We may assume that,
egalitarian goals notwithstanding, the opinions of people who have
distinguished themselves in some way will naturally carry more
weight. Or we may become concerned that the outcome of the
group's work will not be of the high caliber that we, ourselves, feel
capable of achieving. Others among us may readily accede to
individuals who seem knowledgeable and capable of taking on
challenging problems, and may even frown on those who don't allow
themselves to be molded, further alienating individuals who
challenge the leadership.

Many conflicts arise out of the desire to control other people's
behavior and to control the output of the group's activities. Whenever
an attempt is made to manage or direct another member of the
group, no matter how well meaning (to preserve harmony, end
disruption, make time to tend to the work of the group, ensure high
quality, etc.), that person will inevitably feel resentful, and possibly
very hurt or angry. If he or she reacts, conflict begins. Many conflicts
that drag down collectives for months, often resulting in indelible
feuds, could have been prevented if the collective's members were
more willing to tolerate the coexistence of different opinions,
approaches or strategies, objecting only when a fundamental
principle was at stake.

The end result of a project that has been produced collectively is
an uneven patchwork of viewpoints and ability levels. Making room
for everybody to contribute, even when ability is not equal, is a
strength, not a weakness; so is letting the process show. We are
accustomed to valuing a slick, polished presentation, but if we let the
seams show, this will empower others with information about how
something was put together. If we accept a heterogeneous, bumpy
outcome as a given, before the work even begins, we will avoid a lot
of head-butting further down the road.

Because groups based on consensus and equality presuppose
mutual trust and a shared sense of mission, many of us may expect
solidarity, harmony, and kindness to permeate such groups. To the
contrary, adhering to egalitarian, anti-authoritarian principles means
applying minimal interference to one another, or letting people be
who they are — including the annoying, the trying, and the obnoxious
— and accepting the outcomes as well.


## Cruelty

How we choose to treat each other in a group that is committed
to equality and justice goes to the core of what we hope to
accomplish as activists. If we hope to bring about a fairer, more
compassionate world we have to start with our most basic
interactions. The fact that deliberate cruelty does not lead to greater
justice should be too obvious to mention. Yet in collectives it's very
often considered normal, not even worthy of a mention or of a raised
eyebrow. Tormenting someone mercilessly until they flee the
collective — or even the entire local activist scene because they are
so afraid of encountering further abuse — is common practice. We've
never heard anyone speak up to say that it's morally repugnant or to
try to stop it in any way.

Condoning and accepting cruelty as business-as-usual is an
attitude and a way of living. Its potential for creating and promoting
social injustice and a more vicious, less tolerant world makes it a
matter of the utmost importance: it is our duty and responsibility to
vigorously oppose cruelty within our own midst.

The same behavior we saw as children in school playgrounds,
where an individual is singled out for no other reason than he or she
is an easy mark and is then subjected to a gleeful campaign of
abuse, is much too often at work in our activist collectives. Are we so
conditioned by our upbringing in a society that forces us to conform
to authority that whenever the mantle of established authority is
removed (like it is in an egalitarian collective and in a playground),
we can think of nothing better to do than prey on each other with
cruel name-calling and senseless attacks? Another frequent
consequence of new-found freedom is to immediately establish and
follow new hierarchies based on who is more popular or stronger or
the best at manipulation versus who is unpopular, out of the group's
mainstream, the easy target, etc. It's just like Lord of the Flies...

Individuals who believe they have been mistreated by their
fellow group members feel genuine pain. It is not possible or
appropriate, in our view, to explain away somebody's pain by
pointing to the group's positive work or invoking regulations that the
pariah in question may or may not have properly followed. Do you
honestly believe that anyone deserves to have cruelty visited upon
them? Even if they're a pain in the ass, if they're impossible to deal
with — even if they themselves are cruel — that is no reason to taunt,
torment, bully, slander with vicious lies, etc. As activists, we hope to
create a world in which difficulties can be addressed and every
attempt is made to resolve them, not one where suppression,
intimidation, and violence (psychological or physical violence) are
resorted to if the group's majority or most vocal members do not get
their way.

It is not possible, in our view, for the person who feels pushed
out or abused to simply be mistaken in perceiving a sustained
campaign of attacks and vilification by the group (or a faction of the
group) against him/herself. The hurt that is expressed over and over
in situation after situation is undoubtedly real, and it should not be
dismissed, regardless of whether or not the person experiencing it
was originally (or continues to be) at fault.

Regardless of the merits or faults present in each situation, it's
not okay for us to inflict emotional pain on one another. That should
be a basic tenet.

A commitment to compassion and justice and against cruelty
(yes, that's what it is) needs to be overtly stated as the basis for how
an egalitarian group operates.

We only need to look at the current political situation to see the
wages of indifference and casual acceptance of cruelty. Once we
have relinquished our moral compass, we can condone both small
and huge moral insults with logical arguments and pragmatism.
Where is the outrage of the American public at the thousands of
deaths and injuries of Iraqi civilians? Even for those who believe the
war to be politically justified, how can ecstatic cheering be the
overwhelming reaction to death, suffering and destruction on a
massive scale? Wouldn't the more human reaction be sober
regretfulness that sometimes harm is done in order to achieve a
purportedly worthwhile objective?

Yet even among the activists who vehemently oppose war,
many do so for political reasons, because they object to imperialism
or other political forces they believe to be at play in this conflict, not
out of moral outrage. And of those who invoke humanitarian
objections to war, many adopt that view as a persuasive arguing
position, not as a deeply held revulsion to causing suffering.

The purpose of activism, fundamentally, is to create a better
world, one where there is greater justice, equality, and harmony and
less pain and hardship. It is not to put forward a particular agenda.
When we overlook this basic truth and allow ourselves to act with
deliberate cruelty toward people in our own collectives, then go on to
justify our actions by saying that we vilified or attacked our comrades
because they were interfering with important political organizing, we
have twisted our motives into an indefensible moral pretzel.

Sometimes, moreover, the individual can be really badly
misunderstood by a group which has made assumptions or followed
presumptions that might not really apply to the person involved. In
judging individuals, groups can make terrible mistakes, sometimes
based on a lot of bias and prejudice. This is illustrated not only by the
countless collectivist mistakes made throughout history, but also by
the many smaller examples of collective injustice and manipulation
that we have already discussed in our Collective Book. When a
group is manipulated, becomes misguided, or simply fails to be
vigilant about judging everyone fairly and equally, it can become
more wrong than any single member.

The individual also might have a particular outlook or opinion in
a given situation that ultimately proves to be wiser or more accurate
than the outlook of the group. This is why it really is necessary to
listen to the opinions of individuals within the group who may not be
going so well with the collective flow. Dissenting opinions sometimes
can change the mind of the entire group, once the group considers
the dissenting opinion fairly, allowing each person within that group
to weigh the merits of each (differing) point of view.

In examining other literature dealing with problems within
collectives, we have seen quite a few articles talking about how to
deal with the difficult person who won't go along with the group, the
ornery person, the malcontent whose behavior or opinions seem to
disrupt the group's smooth functioning. The issue is thus usually
depicted as finding a good way for the group to collectively deal with
a problem member. Unfortunately, this is only one way of looking at
things.

A truly democratic and egalitarian collective can't always
assume that the only problem to be considered in group-versus-
individual conflicts is protecting the integrity of the group against the
disruptive individual. Sometimes, the problem involves protecting the
individual against the group.


## The Collective Is Not Always More Correct Than The Individual


One mistake often made by people who want to strive for a more
collective society — whether that society might be called anarchist,
communist, or "small-d" democratic — is to assume that the
collective can always be trusted above the individual. Unfortunately,
in many radical-left circles, if we talk too much about individual rights
and even suggest that an individual's opinions and observations
might be closer to the truth than the votes or consensus of the
collective, we might be accused of pushing "individualism," which
supposedly is a bad trait typical of capitalist and "bourgeois" society,
not to be tolerated in egalitarian circles. Yet, this kind of mentality, at
least when taken to the extreme, enabled a lot of really nasty
totalitarian societies to exist in the past century, and the history of
those societies basically proves the point that individuals (who were
suppressed) can often be more correct than the group.

If we are really striving for a fair and egalitarian society, then we
need to give utmost importance to the rights and liberties of the
individual. This does not mean promoting the kind of "individualism"
that dictates that each person must look out for her/himself and that
collective decision making and concern for the community are a
hindrance to true liberty. What it does mean is that each of us is
unique and must be considered, judged and observed according to
our own unique combination of circumstances. This means that our
behaviors are far more complex than might be assumed by the knee-
jerk sort of ideologue who would say, for instance, that any of us
enjoys certain privileges above others for belonging to one particular
group based on race, gender, or ethnic origins. It also means that
nobody's behavior should be judged by a formulaic check list, so that
in any given situation, one person must be assumed to have certain
politically undesirable characteristics based on a particular incident
when we don't know the backgrounds, tendencies, or histories of the
individuals involved. (So, for example, a man who shouts at a woman
or says something vaguely disrespectful to her is automatically
assumed to be "sexist" when a closer examination of the histories of
the individuals involved might reveal a dynamic that is far more
complex, with more equal hostilities, etc., than anyone realized.)
When we fail to recognize the potential uniqueness and complexity of
the individual, then we are failing to create a situation in which each
individual might enjoy a maximum amount of freedom and liberty.


## Micro-Managing Other People's Behavior


In a well-intentioned attempt to establish guidelines to prevent
disrespect of one another and abuse of process, some collectives fall
into the authoritarian trap of dictating which specific, often minute,
behaviors collective members may or may not display. Those who do
not strictly adhere to the regulations, perhaps even unwittingly, may
be frowned upon, smarmily chastised, or rendered into undesirables.

Self-appointed leaders who are adept at working the consensus
system can use strict adherence to nit-picking rules as a way to put
themselves up as role models (since they always follow the letter,
though not the spirit, of the rules). Then, they can paint those who
may not be so versed in the minutia of the guidelines, or so slick
about appearing to follow them, as saboteurs of consensus. The
hapless or gauche, who might commit blunders like using
inappropriate terminology or speaking out of turn, thus become easy
victims for the "process tyrants."

Behavioral guidelines cannot substitute for basic respect,
decency, common sense, or an honest attempt to listen, understand,
and strive for fairness. Any attempt to codify and restrict normal
human interactions can create a tightly wound atmosphere of
coercion and disapproval.

### Interrupting

A lot has been made in activist circles about the
inappropriateness of interrupting someone when he or she is talking.
Interrupting is often obnoxious and can be used, sometimes
intentionally, to dominate, but it is also a common human fallacy.
Some people are chronic interrupters: they may be so brimming with
exciting ideas or information that they just can't contain themselves.
Such individuals can usually be handled with joking, light-handed
rebukes or by simply interrupting them in return. Others are long-
winded droners. While everyone should be given their space to
speak, it's not necessarily wrong to gently interrupt those who have
been boring the collective with endless, repetitive speeches. They
should not be silenced, of course, but they can be made aware of the
effects of their verbosity.

Not everybody has the same skill at navigating interpersonal
exchanges. Some people are not good at recognizing that split
second when someone has finished talking and it's okay to jump in.
They are the ones who are most likely to interrupt, and be
reprimanded for it, while they also, ironically, are the least likely to
get a word out and have their opinions heard. While facilitation and
hand-raising should prevent this, there will always be circumstances
when people are engaging in informal conversations, whether in or
out of meetings.

It's also fairly normal, in everyday speech, to interrupt someone
to nip a misunderstanding in the bud: "Oh, no, no. I'm sorry I made it
sound that way. What I meant was...." Collective process needs to
take ordinary interaction into account, not try to dictate actions that
are awkward and artificial, then frown on people who don't
immediately take to them.

### Stacking

Prohibiting any and all interruptions can become a problem at
meetings when added to the strict stipulation that members can only
speak in the order in which they raise their hands. Hand-raising is a
good idea, since it stops people from merely shouting over each
other to be heard, as is making a list, or stack, that determines
whose fair turn it is to talk. Yet, these practices, if applied too rigidly,
can easily stifle discussion or facilitate abuses.

For instance, someone may intentionally make grossly untrue
and damaging statements about a project in an attempt to denigrate
it. The person who made the original proposal may be desperate to
say something, but he mustn't interrupt, and there are others in line
to speak. If the proposal-maker speaks up for himself out of order he
will, in all likelihood, be looked at with opprobrium, only adding to the
denigrator's case that his project is suspect. If he waits until it's his
rightful turn to talk, it may be too late to undo other members'
already-solidifying, inaccurate perceptions. It makes no sense to use
hand-raising merely to make a list without allowing for the fact that
discussions require an exchange. When questions go unanswered or
falsehoods unchallenged, there can be no discourse.

What often happens is that someone will raise his hand to
respond to something that has just been said; by the time it is his
turn to speak there may have been another ten comments made on
other matters, and what the person had raised his hand to say is no
longer on point. Since it will be his only chance to talk, however, he
will still take his turn. Multiply this by the number of people in the
meeting, and you have a random list of utterances on various topics
and no semblance of a discussion.

The door is opened to speech-making by the self-important while
the meek or shy may only get a few words out and not receive
another opportunity to explain themselves more fully.

There has to be some way for people to be allowed to clear the
air when necessary without exposing themselves to outraged
censure.

### Prioritizing

Many collectives have made rules that require facilitators to give
priority to members of traditionally oppressed groups. While the
intention is commendable, in practice it's not an easy task to
determine which individuals in a particular group are more or less
likely to be overlooked or silenced. Power inequities within a small
group of human beings can stem from a great many factors that are
not easily reduced to race, class, or gender. Thus, anyone who
attempts to combat injustice by applying overly simplistic criteria
might actually perpetuate even more injustice. And many collectives,
from what we have seen, need to be more conscious about avoiding
that kind of mistake.

It is important to make sure that those who have been quiet get
a chance to be heard. But, once again, the rule must not be applied
in the absence of common sense. Everyone should feel free to say,
"I have no comment," without being made out to be a deferrer to
oppressors. In addition, people who are directly involved in a given
issue, or are themselves raising a matter for the group to consider,
are likely to have more to say when it comes up for discussion and
may even be questioned by the group to elucidate and clarify
relevant points. They should not be silenced because someone else
has not said as much on the topic. It makes no sense for someone
who brings up a concern to be prohibited from participating in the
ensuing discussion simply because he or she has used up the
allotted speaking time.


## Skepticism is Healthy



Being skeptical is not the same as being distrustful or
suspicious, both of which can undermine a collective's honest
interactions, as well as play tricks with one's own judgment. It simply
means not jumping to conclusions, neither positive nor negative,
before having investigated an issue.

Coming to a hasty, negative opinion of another person, as many
of us know, is often ugly and can turn out to be grossly unfair.
Furthermore, since most of us don't like to admit it when we're
wrong, the bad reputation can actually persist even after the facts
have proven the condemnation to be unwarranted. But a thoughtless
positive judgment can be damaging too. We might give somebody's
words too much importance, because she gives the impression of
being exceptionally knowledgeable or effective, for instance, and
unwittingly follow unwise advice or even turn over control of the
group (always a bad idea).

Some of the most despicable injustices that happen in
collectives are perpetrated by those of us who were only trying to
help. A fellow collective member comes up to you, clearly upset and
outraged, and tells you about someone who's been making his or her
life hell. As a good friend, your reaction is probably to sympathize,
listen, and ask what you can do. You may even take it upon yourself
to alert others of the problem. Thus, the wheels of a rumor or — worse
— a baseless character assassination, have just been set in motion.
By you.

We are not suggesting that you be stingy with your sympathy
and emotional support, only that you keep in mind that every story
has two sides, and that it's usually not prudent to act until the matter
has been explored a little more thoroughly. In many cases, whenever
two sides of a story are clearly divergent and emotions are running
high, it's best to begin a formal grievance or conflict resolution
proceeding.

It's not uncommon for members who feel they have been
aggrieved in some way to circulate a petition, asking other members
to sign off on some kind of sanction against the presumed
transgressor, whether it's a temporary ban or a demand they seek
counseling. In our experience, people are generally all too happy, in
an effort to be supportive and mindful of the best interests of the
group, to sign their names to an accusation about which they have
absolutely no first-hand knowledge, sometimes even excoriating a
person they have never met. Needless to say, this is not a sign of
healthy group dynamics. Even if the persons doing the signing are
well meaning, they are abdicating their responsibilities to the
collective by acting without having done their homework. And those
circulating the petition may feel they have been genuinely wronged,
but they are circumventing group process when they bypass due
process and an open forum for the airing of complaints.
Unfortunately, we have also seen instances in which getting rid of
someone is an intentional, calculated act, where the group is
manipulated into believing it is acting in the collective best interest by
participating in an undemocratic ostracism.

Ironically, a converse kind of phenomenon is also not
uncommon, where a member who has had to tolerate victimization
and abuse by someone in the group seeks help from the collective
and is roundly ignored. Personal power politics tend to come into
play in these cases: an unpopular or not highly regarded person who
complains about someone who is seen as a leader or a more valued
member may find himself alone and a target for ridicule. The proper
way for the group to proceed in either circumstance (whether they
believe the accused or the accuser) is to investigate the situation,
call for formal procedures, such as previously agreed-on conflict
resolution protocols, and allow all parties to air their concerns.
Regardless of who you believe to be right or wrong — whether it's the
defendant or the complainant — making hasty judgments never
serves the interests of fairness. Neither does calling for sanctions
(such as ad hoc banning, the popular favorite) which are excessive
or not necessary for resolving a given circumstance.

It may not be possible to know exactly what the truth is in a
particular situation, but one can come to an educated judgment
based on ascertainable facts and the probable likelihood of certain
events having taken place rather than others, for instance by
considering the motivation that someone might have to dissemble or
stretch the truth.

