# Traducciones

Este repositorio es para realizar traducciones de textos afines al hacklab.

## Protocolo de traducción

1. Crear un directorio por cada texto a traducir.

2. El texto fuente se debe subir en formato markdown para poder convertirlo a
otros formatos usando markdown.

3. Crear issues para cada sección del texto a traducir.

4. Todas las personas que trabajen sobre una sección deben asignarse al issue.

5. Las ediciones finales deben linkearse en esta página.

## Textos traducidos

## Textos en traducción

[Collective Process - Overcoming Power](https://git.rlab.be/seykron/traducciones/blob/master/collective-process/collective_process_overcoming_power.md)

